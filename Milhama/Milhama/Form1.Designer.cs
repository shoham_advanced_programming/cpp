﻿namespace Milhama
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MyScore = new System.Windows.Forms.Label();
            this.OpponentScore = new System.Windows.Forms.Label();
            this.give_up = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // MyScore
            // 
            this.MyScore.AutoSize = true;
            this.MyScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.MyScore.Location = new System.Drawing.Point(24, 13);
            this.MyScore.Name = "MyScore";
            this.MyScore.Size = new System.Drawing.Size(99, 18);
            this.MyScore.TabIndex = 1;
            this.MyScore.Text = "Your Score: 0";
            // 
            // OpponentScore
            // 
            this.OpponentScore.AutoSize = true;
            this.OpponentScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.OpponentScore.Location = new System.Drawing.Point(920, 13);
            this.OpponentScore.Name = "OpponentScore";
            this.OpponentScore.Size = new System.Drawing.Size(133, 18);
            this.OpponentScore.TabIndex = 2;
            this.OpponentScore.Text = "Opponent Score: 0";
            // 
            // give_up
            // 
            this.give_up.Location = new System.Drawing.Point(12, 422);
            this.give_up.Name = "give_up";
            this.give_up.Size = new System.Drawing.Size(75, 25);
            this.give_up.TabIndex = 3;
            this.give_up.Text = "give up";
            this.give_up.UseVisualStyleBackColor = true;
            this.give_up.Click += new System.EventHandler(this.give_up_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lime;
            this.ClientSize = new System.Drawing.Size(1108, 459);
            this.Controls.Add(this.give_up);
            this.Controls.Add(this.OpponentScore);
            this.Controls.Add(this.MyScore);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label MyScore;
        private System.Windows.Forms.Label OpponentScore;
        private System.Windows.Forms.Button give_up;
    }
}

