﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;  

namespace Milhama
{
    public partial class Form1 : Form
    {
        System.Windows.Forms.PictureBox BluePic;
        private Thread serverInputThread;
     
        private Random randCard;
        private int index;
        private string[] cardType = { "clubs", "diamonds", "hearts", "spades" };

        private bool first = false;
        private byte[] inputMessage;
        private byte[] outputMessage;

        NetworkStream clientStream;

        public Form1()
        {
            InitializeComponent();
            inputMessage = new byte[4];
            outputMessage = new byte[4];
            serverConnection();
            GenerateCards();
            randCard = new Random();
        }

        private void serverConnection()
        {
            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
            client.Connect(serverEndPoint);
            clientStream = client.GetStream();
            outputMessage = new ASCIIEncoding().GetBytes("0000");
            clientStream.Write(outputMessage, 0, outputMessage.Length);
            clientStream.Flush();
           
            clientStream.Read(inputMessage, 0, 4);
            
            serverInputThread = new Thread(new ThreadStart(serverInput));
            serverInputThread.Start();
           
        }
        private void serverInput()
        {
            int inputcard;
            while(true)
            {
                clientStream.Read(inputMessage, 0, 4);
                char[] t;
                inputcard = (inputMessage[2]-48)*10 + (inputMessage[3]-48);
                BluePic.Image = (System.Drawing.Image)global::Milhama.Properties.Resources.ResourceManager.GetObject("_" + ((inputcard % 13) + 1) + "_of_" + cardType[inputcard % 4]);
                if (first == false)
                    first = true;
                else
                {
                    if (index < inputcard)
                    {
                        t = MyScore.Text.ToCharArray();
                        t[t.Length - 1] = Convert.ToChar((Convert.ToInt32(t[t.Length - 1]) + 1));
                        Invoke((MethodInvoker)delegate { MyScore.Text = new string(t); });
                    }
                        
                    else
                    {
                        t = OpponentScore.Text.ToCharArray();
                        t[t.Length - 1] = Convert.ToChar((Convert.ToInt32(t[t.Length - 1]) + 1));
                        Invoke((MethodInvoker)delegate { OpponentScore.Text = new string(t);});
                    }
                    
                    first = false;
                }
            }
            
        }
       

        private void GenerateCards()
        {
            //create blue card
            BluePic = new PictureBox();
            BluePic.Name = "picDynamicBlue"; 
            BluePic.Image = global::Milhama.Properties.Resources.card_back_blue;
            BluePic.Location = new Point(415,60);
            BluePic.Size = new System.Drawing.Size(100, 114);
            BluePic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;


            // add the picture object to the form (otherwise it won't be seen)
            this.Controls.Add(BluePic);


            // set the location for the card
            Point nextLocation = new Point(BluePic.Location.X - BluePic.Size.Height * 4 + 50, BluePic.Location.Y + BluePic.Size.Height + 100);

            for (int i = 0; i < 10; i++)
            {
                // create the image object itself
                System.Windows.Forms.PictureBox currentPic = new PictureBox();
                currentPic.Name = "picDynamic" + i; // in our case, this is the only property that changes between the different images
                currentPic.Image = global::Milhama.Properties.Resources.card_back_red;
                currentPic.Location = nextLocation;
                currentPic.Size = new System.Drawing.Size(100, 114);
                currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

                // assign an event to it
                currentPic.Click += delegate(object sender1, EventArgs e1)
                                        {
                                            char[] t;
                                            //grilling a card
                                            int index = randCard.Next(0, 51);
                                            ((PictureBox)sender1).Image = (System.Drawing.Image)global::Milhama.Properties.Resources.ResourceManager.GetObject("_"+ ((index % 13) + 1) + "_of_" + cardType[index%4]);
                                            
                                            //prepering the message
                                            string temp = "0"+index;
                                            if (temp.Length < 3) temp = "0" + temp;
                                            temp = "1" + temp;
                                            outputMessage = new ASCIIEncoding().GetBytes(temp.ToCharArray());

                                            //sending the message to the server              
                                            clientStream.Write(outputMessage, 0, outputMessage.Length);
                                            clientStream.Flush();
                                            if (first == false)
                                                first = true;
                                            else
                                            {
                                                if (index < ((inputMessage[3] - 48) + (inputMessage[2] - 48) * 10))
                                                {
                                                    t = MyScore.Text.ToCharArray();
                                                    t[t.Length - 1] = Convert.ToChar((Convert.ToInt32(t[t.Length - 1]) + 1));
                                                    Invoke((MethodInvoker)delegate { MyScore.Text = new string(t);});
                                                }
                                                    
                                                else
                                                {
                                                    t = OpponentScore.Text.ToCharArray();
                                                    t[t.Length-1] = Convert.ToChar((Convert.ToInt32(t[t.Length-1]) + 1));
                                                    Invoke((MethodInvoker)delegate { OpponentScore.Text = new string(t); });
                                                }
                                                    
                                                first = false;
                                            }
                                            
                                            
                                        };

                // add the picture object to the form (otherwise it won't be seen)
                this.Controls.Add(currentPic);

                // calculate the location point for the next card to be drawn/added
                nextLocation.X += currentPic.Size.Width + 10;
            }
        }

        private void give_up_Click(object sender, EventArgs e)
        {
            this.FormClosed += delegate { MessageBox.Show("You Lost"); };
            this.Close();
        }
    }
}
