﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace birthday_calendar
{
    public partial class Login_window : Form
    {
        public Login_window()
        {
            InitializeComponent();
        }

        private void Login_window_Load(object sender, EventArgs e)
        {

        }

        private void login_click(object sender, EventArgs e)
        {
            this.Hide(); //hiding the login window
            bool IsRegistered = false; 
            string userName = "there is a bug";

            //checking if the user is reginstered 
            try
            {
                string[] text = File.ReadAllLines(@"Users.txt");
                foreach (string i in text)
                {
                    if (i.Split(',')[0].Equals(user_name_input.Text) && i.Split(',')[1].Equals(passward_input.Text))
                    {
                        userName = i.Split(',')[0];
                        IsRegistered = true;
                        break;
                    }
                }
                if (IsRegistered) //if the user is a reginstered user opening the calendar window
                {
                    birthday_calendar bc = new birthday_calendar(userName);
                    bc.ShowDialog();
                }
                else //if not showing an error message
                {
                    MessageBox.Show("Wrong user name or passward");
                }
            }
            catch (IOException) //if failed to open the users file
            {
                MessageBox.Show("Cant find users.txt file");
            }

            this.Close();
        }
        //cancel button for closing the program
        private void cancel_click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
