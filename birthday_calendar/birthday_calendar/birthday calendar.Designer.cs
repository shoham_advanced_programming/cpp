﻿namespace birthday_calendar
{
    partial class birthday_calendar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.monthCalendar = new System.Windows.Forms.MonthCalendar();
            this.textBox = new System.Windows.Forms.Label();
            this.userBox = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // monthCalendar
            // 
            this.monthCalendar.Location = new System.Drawing.Point(18, 64);
            this.monthCalendar.Name = "monthCalendar";
            this.monthCalendar.TabIndex = 0;
            this.monthCalendar.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar_DateChanged);
            // 
            // textBox
            // 
            this.textBox.AutoSize = true;
            this.textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.textBox.ForeColor = System.Drawing.SystemColors.Highlight;
            this.textBox.Location = new System.Drawing.Point(14, 247);
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(70, 20);
            this.textBox.TabIndex = 1;
            this.textBox.Text = "no event";
            // 
            // userBox
            // 
            this.userBox.AutoSize = true;
            this.userBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userBox.ForeColor = System.Drawing.SystemColors.Highlight;
            this.userBox.Location = new System.Drawing.Point(140, 29);
            this.userBox.Name = "userBox";
            this.userBox.Size = new System.Drawing.Size(0, 26);
            this.userBox.TabIndex = 2;
            // 
            // birthday_calendar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(490, 314);
            this.Controls.Add(this.userBox);
            this.Controls.Add(this.textBox);
            this.Controls.Add(this.monthCalendar);
            this.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.Name = "birthday_calendar";
            this.Text = "birthday_calendar";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.birthday_calendar_FormClosed);
            this.Load += new System.EventHandler(this.birthday_calendar_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar monthCalendar;
        private System.Windows.Forms.Label textBox;
        private System.Windows.Forms.Label userBox;
    }
}