﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace birthday_calendar
{
    public partial class birthday_calendar : Form
    {
        private string user_name;
        private Dictionary<string, string> dic; //a dictionary cointaining the birthdays dates
   
        private string[] content;
      
        public birthday_calendar(string name)
        {
            InitializeComponent();
            user_name = name;

            //Initializing the dictionary
            if (File.Exists(user_name + "DB.txt"))
            {
                content = File.ReadAllLines(user_name + "DB.txt");
                dic = new Dictionary<string, string>();

                foreach(string i in content)
                    dic.Add(i.Split(',')[1], i.Split(',')[0]);
            }
            else
            {
                File.Open(user_name + "DB.txt", FileMode.CreateNew);
            }
        }

        private void birthday_calendar_Load(object sender, EventArgs e)
        {
            userBox.Text = "welcome " + user_name + "!";
        }

        private void birthday_calendar_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
        }

        private void monthCalendar_DateChanged(object sender, DateRangeEventArgs e)
        {
            //checking if there is a birthday on the given date
            string i = monthCalendar.SelectionStart.ToString().Split(' ')[0];
            if(dic.ContainsKey(i))
            {
                textBox.Text = dic[i] + " has a birthday on " + i;
            }
            else
            {
                textBox.Text = "no event";
            }
        }

    }
}
