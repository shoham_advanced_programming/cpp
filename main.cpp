#pragma once

#include <Windows.h>
#include <iostream>
#include <strsafe.h>
#include <string>
#include <vector>
#include "Helper.h"

#define MAX_PATH 2048
using namespace std;

void run(vector<string> cmd)
{
	if (cmd[0] == "pwd")
	{
		TCHAR buffer[MAX_PATH] = { 0 };
		if (!GetCurrentDirectory(MAX_PATH, buffer))
		{
			cout << "pwd call failed: " + GetLastError() << endl;
			return;
		}
		cout << buffer << endl;
	}
	else if (cmd[0] == "cd")
	{
		if (cmd.size() == 2 && !SetCurrentDirectory(cmd[1].c_str()))
		{
			cout << "SetCurrentDirectory failed:" + GetLastError() << endl;
			return;
		}
	}
	else if (cmd[0] == "create")
	{
		if (cmd.size() == 2)
			CreateFile(cmd[1].c_str(), GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
	}
	else if (cmd[0] == "ls")
	{
		HANDLE hFind = INVALID_HANDLE_VALUE;
		WIN32_FIND_DATA ffd; TCHAR buffer[MAX_PATH] = { 0 };

		GetCurrentDirectory(MAX_PATH, buffer); 
		StringCchCat(buffer, MAX_PATH, TEXT("\\*"));
		hFind = FindFirstFile(buffer, &ffd);
		while (FindNextFile(hFind, &ffd) != 0)
			cout << ffd.cFileName << endl;
		FindClose(hFind);
	}
}

int main()
{
	string input;
	vector<string> cmd;

	//getting input from user
	getline(cin, input);
	Helper::trim(input);

	while (input != "exit")
	{
		//analyzing the input to command and parameters
		cmd = Helper::get_words(input);
		run(cmd);

		//getting input from user
		getline(cin, input);
		Helper::trim(input);
	}
	
	return 0;
}
